import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.LookUpTable;
import ij.Macro;
import ij.Menus;
import ij.Prefs;
import ij.WindowManager;
import ij.measure.Calibration;
import ij.plugin.CanvasResizer;
import ij.plugin.PlugIn;
import ij.plugin.StackCombiner;
import ij.plugin.filter.RankFilters;
import ij.gui.DialogListener;
import ij.gui.GenericDialog;
import ij.gui.NonBlockingGenericDialog;
import ij.gui.Plot;
import ij.io.FileInfo;
import ij.io.ImageWriter;
import ij.io.OpenDialog;
import ij.io.Opener;
import ij.io.RoiEncoder;
import ij.io.TiffDecoder;
import ij.io.TiffEncoder;
import ij.process.ByteProcessor;
import ij.process.ColorProcessor;
import ij.process.FloatProcessor;
import ij.process.ImageConverter;
import ij.process.ImageProcessor;
import ij.process.ShortProcessor;
import ij.process.StackProcessor;
import ij.plugin.frame.Recorder;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.tree.DefaultMutableTreeNode;

import ncsa.hdf.object.*; // the common object package
import ncsa.hdf.object.h5.*; // the HDF5 implementation
import ncsa.hdf.hdf5lib.*;
import ncsa.hdf.hdf5lib.exceptions.HDF5Exception;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.awt.AWTEvent;
import java.awt.Button;
import java.awt.Checkbox;
import java.awt.Choice;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.Rectangle;
import java.awt.Scrollbar;
import java.awt.TextComponent;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import java.awt.image.ColorModel;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;
import java.util.Map.Entry;


public class MBIRScriptWriter2 implements PlugIn, ActionListener, TextListener, DialogListener {
	GenericDialog gd2 = new NonBlockingGenericDialog("Reconstruction");
	String rootName;
	String directory;
	String lastDir;
	String pluginPath;
	String walltime = "12:00:00";
	//got some ideas for this from http://dev.loci.wisc.edu/trac/software/browser/trunk/components/loci-plugins/src/loci/plugins/importer/ImporterDialog.java?rev=6039#L403

	TextField resolution_field;
	TextField width_field;
	TextField recon_field;
	TextField dark_field;
	TextField bright_field;
	TextField start_field;
	TextField slices_field;
	TextField projection_field;
	TextField pixel_field;
	TextField rot_field;
	TextField smooth_field;
	TextField zinger_field;
	TextField variance_field;
	TextField threshold_field;
	TextField subsampling_field;
	TextField cor_field;

	TextField input_field;
	TextField dataset_field;
	TextField error_field;
	TextField output_field;
	TextField walltime_field;
	
	Checkbox rotation_box;
	Checkbox interval_box;
	Vector the_checkboxes2;
	Vector the_numbers2;
	Vector the_strings2;
	float rotCenter;
	double pixSize;
	int resolution = 2560;
	float smooth = (float) 0.3;
	float zinger = (float) 4.0;
	int start;
	int end;
	
	boolean inputIsH5;
	boolean recon360 = false;
	boolean interval_bright = false;

	H5File inFile;
	String scriptName;
	String datasetName;

	int nangles=0;
	int i0cycle=0;
	int nslices=0;
	int effective_slices;
	int firstImage = 0;
	int zeropad = 4;
	int roi_top = 0;
	int roi_left = 0;
	int yinit;
	int xwidth;
	int reconwidth;
	int yheight;
	int zelements;
	int dark_slices;
	int bright_slices;
	double lastAngle; 
	int variance;
	int threshold;
	int subsampling;
	
	@Override
	public boolean dialogItemChanged(GenericDialog gd, AWTEvent e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void textValueChanged(TextEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void run(String arg) {
		directory = getTheInputName();
		if(directory == "")
		{
			return;
		}else
		{
			if(inputIsH5)
			{
				inFile = new H5File(directory); 
				lastDir = directory.substring(0, directory.lastIndexOf(File.separator)+1);
				datasetName = getDatasetName();
				try{
					getReconParamFromSourceFile(directory);
				}catch(Exception e1){
					IJ.log(e1.toString());
				}
				makeGUI();
				gd2.showDialog();
				if (gd2.wasCanceled()){return;}
				else {
					return;
				}
			}
			else {
				IJ.log("Requires H5 file to proceed");
				return;
			}
		}
	}
	public static void main(String[] args){
		new MBIRScriptWriter2().run(null);
	}
	
	private String getDatasetName(){
		 String tempDatasetName = "";
		 List<String> datasets = new ArrayList<String>();
		 
		 try{
			 inFile.open();
			 DefaultMutableTreeNode ret = (DefaultMutableTreeNode) inFile.getRootNode();
			 DefaultMutableTreeNode child = (DefaultMutableTreeNode) ret.getChildAt(0);
			 Group temp = (Group) child.getUserObject();
			 List<HObject> memberList = temp.getMemberList();
			 
			 //System.out.println("\nmemberList List "+(Arrays.toString(memberList.toArray())));
			 
			 for (HObject hObject: memberList) {
				 if (hObject instanceof Group) {
					 datasets.add(((Group) hObject).getFullName());
				 } else if (hObject instanceof Dataset) {
				 }
			 }
			 
			 if (datasets.size() == 0) {
					datasets.add(temp.getFullName());
			 }
			 
			 String[] datasetsArr = new String[datasets.size()];
			 datasetsArr = datasets.toArray(datasetsArr);
			 
			 //System.out.println("\ndataset List "+(Arrays.toString(datasets.toArray())));
			 
			 GenericDialog datasetGD = new GenericDialog("Select a Dataset");
			 datasetGD.setOKLabel("Select");
			 datasetGD.setSize(300,300);
			 datasetGD.centerDialog(true);
			 datasetGD.addDialogListener(this);
			 datasetGD.addMessage("Datasets Contained in H5File:");
			 datasetGD.addChoice("", datasetsArr, datasetsArr[0]);
			 datasetGD.showDialog();
			 
			 if(datasetGD.wasOKed()){
				 tempDatasetName = datasetGD.getNextChoice();
			 }
		 }catch(Exception ex){
			 ex.printStackTrace();
		 }
		 return tempDatasetName;
	}
	/* String getTheInputName(String startingDirectory)
	* ==================================================
	* Pops up a file chooser dialog to allow the user to select either an H5 file or
	* a directory containing tiffs as the input.
	*/
	private String getTheInputName(){
		String input = "";
		JFileChooser fc = new JFileChooser(OpenDialog.getLastDirectory());
		//FileNameExtensionFilter filter = new FileNameExtensionFilter("H5 File", "h5 file", "h5", "hdf5");
		fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		//fc.setFileFilter(filter);
		fc.setDialogTitle("Select H5 File");
		fc.showDialog(null, "Select");
		input = fc.getSelectedFile().getAbsolutePath();
		lastDir = fc.getSelectedFile().getPath().substring(0,fc.getSelectedFile().getPath().lastIndexOf(Prefs.separator)+1);
		OpenDialog.setLastDirectory(lastDir);
		
		if (Menus.getPlugInsPath()==null){
				pluginPath = "C:\\";
		} else
			pluginPath = Menus.getPlugInsPath();
		
		if(fc.getSelectedFile().isDirectory()){
			inputIsH5 = false;
		}else{
			if (input.contains(".h5")){
				inputIsH5 = true;
			}
			else {
				return input;
			}
		}
		if(input == null){
			input = "";
		}
	    return input;
	}
	public Properties parseSct(File sctFile) {
		Properties props = new Properties();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(sctFile));
			props.load(reader);
		} catch (IOException ioe) {
			ioe.printStackTrace(System.err);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException ioe) {
				}
			}
		}
		return props;
	}
	
	/* getMetaDataFromH5(File infile)
	 * =======================================================
	 * This function takes an H5 file (infile) and does the initial opening/processing.
	 * If the initial group contained in the H5 file is the selected datasetName,
	 * the function reads in the meta data into a map and sets the corresponding variables
	 * to their appropriate values.
	 * If the initial group isn't the correct dataset, the companion function
	 * getMetaDataFromH5(Group group) is called to recursively search further into the H5
	 * file for the correct dataset (necessary for tiled datasets, possible for future ALS
	 * H5 files that may have a different tree structure).
	 */
	void getMetaDataFromH5(File infile){		
		try{
			inFile.open();
			Map<String, String> inputAttributeMap = new HashMap<String, String>();
			DefaultMutableTreeNode ret = (DefaultMutableTreeNode) inFile.getRootNode();
			DefaultMutableTreeNode child = (DefaultMutableTreeNode) ret.getChildAt(0);
			Group group = (Group) ret.getUserObject();
			
			if(datasetName.equals(group.getFullName())){
				@SuppressWarnings("unchecked")
				List<Attribute> meta = group.getMetadata();
				for (Attribute attr: meta) {
					if (attr.getType().getDatatypeClass() == Datatype.CLASS_STRING) {
						Object[] vals = (Object[]) attr.getValue();
						inputAttributeMap.put(attr.getName(), (String) vals[0]);
					} 
					else if (attr.getType().getDatatypeClass() ==  Datatype.CLASS_INTEGER
						&& attr.getType().getDatatypeSize() == Long.SIZE / 8) 
					{
						long[] vals = (long[]) attr.getValue();
						inputAttributeMap.put(attr.getName(), Long.toString(vals[0]));
					} else {
							throw new Exception("Don't know how to read attr " + attr.getName());
					}
				}
				
				List<HObject> memberList = group.getMemberList();
				bright_slices = 0; //reinitialize variables
				dark_slices = 0; //reinitialize variables
				for (HObject obj: memberList) { //for objects in List memberList//
					if (obj.getName().contains("bak_")) {
						bright_slices++;
					} else if (obj.getName().contains("drk_")) {
						dark_slices++;
					}
				}
				
				rootName = inputAttributeMap.get("object");
				String stringchecker = rootName;
				
				nangles = Integer.parseInt(inputAttributeMap.get("nangles"));
				i0cycle = Integer.parseInt(inputAttributeMap.get("i0cycle"));
				xwidth = Integer.parseInt(inputAttributeMap.get("rxelements"));
				yheight = Integer.parseInt(inputAttributeMap.get("rzelements"));
				pixSize = Float.parseFloat(inputAttributeMap.get("pxsize"));
				lastAngle = Double.parseDouble(inputAttributeMap.get("arange"));
				
				pixSize=pixSize*1000;
		
			}else{
				List<HObject> memberList = group.getMemberList();
				for (HObject hObject: memberList) 
				{
					if (hObject instanceof Group) {
						getMetaDataFromH5Group((Group) hObject);
					} 
				}
			}
			inFile.close();
		}catch(Exception ex){}finally{}
	}
		 
	/* getMetaDataFromH5(Group group)
	* =================================================
	* Checks if the current group within the H5 file is the required dataset.
	* If so, it reads the meta data into a map, and sets the necessary recon variables to
	* the values in the H5 file.
	* If not, calls this function recursively on all groups within group.  This is necessary
	* to handle tiled datasets (H5 structure is one level deeper).
	*/
	void getMetaDataFromH5Group(Group group){
		System.out.printf("\ndatasetName = %s\ngroup.getFullName() = %s\n", datasetName, group.getFullName());
		if(datasetName.equals(group.getFullName())){
			Map<String, String> inputAttributeMap2 = new HashMap<String, String>();
			try{
				@SuppressWarnings("unchecked")
				List<Attribute> meta = group.getMetadata();
				//System.out.println(Arrays.toString(meta.toArray())); //[All attributes from group will be composed into a list and printed as an array]
				
				for (Attribute attr: meta) { //for attributes in List meta//
					if (attr.getType().getDatatypeClass() == Datatype.CLASS_STRING) {
						Object[] vals = (Object[]) attr.getValue();
						inputAttributeMap2.put(attr.getName(), (String) vals[0]);
						//System.out.println((String) attr.getValue()+'\n');
					} else if (attr.getType().getDatatypeClass() ==  Datatype.CLASS_INTEGER
							&& attr.getType().getDatatypeSize() == Long.SIZE / 8) 
					{
						long[] vals = (long[]) attr.getValue();
						inputAttributeMap2.put(attr.getName(), Long.toString(vals[0]));
					} else {
							throw new Exception("Don't know how to read attr " + attr.getName());
					}
				}
				
				List<HObject> memberList = group.getMemberList();
				//System.out.println(Arrays.toString(memberList.toArray())); //[All attributes from group will be composed into a list and printed as an array]
				bright_slices = 0; //reinitialize variables
				dark_slices = 0; //reinitialize variables
				//Search and count bright and dark images in dataset//
				for (HObject obj: memberList) { //for objects in List memberList//
					//System.out.println(obj.getName());
					
					if (obj.getName().contains("bak_")) {
						bright_slices++;
					} else if (obj.getName().contains("drk_")) {
						dark_slices++;
					}
				}
				
				System.out.println(new AttributeMap<String, String>(inputAttributeMap2));

				rootName = inputAttributeMap2.get("object");

				nangles = Integer.parseInt(inputAttributeMap2.get("nangles"));
				i0cycle = Integer.parseInt(inputAttributeMap2.get("i0cycle"));
				pixSize = Float.parseFloat(inputAttributeMap2.get("pxsize"));
				xwidth = Integer.parseInt(inputAttributeMap2.get("rxelements"));
				yheight = Integer.parseInt(inputAttributeMap2.get("rzelements"));
				lastAngle = Double.parseDouble(inputAttributeMap2.get("arange"));
				pixSize=pixSize*1000;
		
			}catch(Exception ex){}
			
		}else{
			List<HObject> memberList = group.getMemberList();
			for (HObject hObject: memberList) {
				if (hObject instanceof Group) {
					getMetaDataFromH5Group((Group) hObject);
				} 
			}
		}
	}

	
	public void getReconParamFromSourceFile(String directory) throws Exception{
		if(inputIsH5){
			getMetaDataFromH5(inFile);
		}else{
			
			//First, we will create filters based on extension types expected to be within the input directory//
			//if input directory is not H5, it should have 1 sct file and sequences of tiff images.//
			//Tiff images can then be further distinguished as drk images or bak images//
			
			
			File sourcedirectory = new File(directory);
			FilenameFilter filter_sct1 = new sctfilter();
			String[] filelist_sct = sourcedirectory.list(filter_sct1);
			FilenameFilter filter_tif1 = new tiffilter();
			String[] filelist_tif = sourcedirectory.list(filter_tif1);
			FilenameFilter filter_drk1 = new drkfilter();
			String[] filelist_drk = sourcedirectory.list(filter_drk1);
			FilenameFilter filter_bak1 = new bakfilter();
			String[] filelist_bak = sourcedirectory.list(filter_bak1);
			FilenameFilter filter_csv1 = new csvfilter();
			String[] filelist_csv = sourcedirectory.list(filter_csv1);
			FilenameFilter filter_tifNOtbakNOTdrk1 = new tifNOTbakNOTdrkfilter();
			String[] filelist_tifNOTbakNOTdrk = sourcedirectory.list(filter_tifNOtbakNOTdrk1);
			
			//Next, we will use the sct filter above to detect files and read parameters in the .sct file.//
			//Parameters include number of angles, 0th cycle, sample exposure, whether bright exposure was used//
			//width/height of the image, photon energy, pixel size//  
			
			if (filelist_sct.length > 0){
				rootName = filelist_sct[0].substring(0,filelist_sct[0].lastIndexOf("."));

				File sctFile = new File(directory+rootName+".sct");
				Properties sctContents = parseSct(sctFile);

				nangles = Integer.parseInt(sctContents.getProperty("-nangles"));
				i0cycle =  Integer.parseInt(sctContents.getProperty("-i0cycle"));
				pixSize = Float.parseFloat(sctContents.getProperty("-pxsize"));
				xwidth =  Integer.parseInt(sctContents.getProperty("-rxelements"));
				yheight = Integer.parseInt(sctContents.getProperty("-rzelements"));
				pixSize=pixSize*1000;

				//yinit is never specifically initialized, so it'll carry a default value of 0//
				//Nevertheless, this function will have a failsafe in situations where yinit is larger than yheight for some reason//
				
				if(yinit > yheight)
					yinit = 0;

				lastAngle = Double.parseDouble(sctContents.getProperty("-arange"));
				
			//if we DID NOT have a SCT file in our input directory, then we will pull our rootname from a tiff file and certain parameters based from them//
			//if we do not have any tiff files, the rootname will default to "", which means we really don't have anything to do.//
				
			}else if (filelist_tif.length > 0){
				if (filelist_tifNOTbakNOTdrk[0].lastIndexOf("_0") > 0){
					rootName = filelist_tifNOTbakNOTdrk[0].substring(0,filelist_tifNOTbakNOTdrk[0].lastIndexOf("_0"));

				}
				else{
					rootName = filelist_tifNOTbakNOTdrk[0].substring(0,filelist_tifNOTbakNOTdrk[0].lastIndexOf("0"));
				}
					nangles = filelist_tif.length-filelist_bak.length-filelist_drk.length;
					i0cycle = 0;
			}else{
				rootName = "";
			}
			
			//Count the dark_slices and bright_slices and put them into well-named variables for use//
			
			dark_slices = filelist_drk.length;
			bright_slices = filelist_bak.length;
		
		}
		if (lastAngle > 180){
			recon360 = true;
			IJ.log("360 degree dataset detected!");
		}
		if (i0cycle > 0){
			interval_bright = true;
			IJ.log("Intebright dataset detected!");
		}
	}
	
	
	private void makeGUI(){
		gd2.setSize(600,700);
		gd2.centerDialog(true);

		gd2.addStringField("Input File",directory,50);
		gd2.addStringField("Dataset Name", datasetName,50);
		gd2.addStringField("Error Log", datasetName+"$PBS_JOBID.err",50);
		gd2.addStringField("Output Log", datasetName+"$PBS_JOBID.out",50);
		gd2.addStringField("Wall Time",walltime,10);
		gd2.addNumericField("X width",xwidth,0);
		gd2.addNumericField("Resolution",resolution,0);
		gd2.addNumericField("Dark Slices Counter",dark_slices,0);
		gd2.addNumericField("Bright Slices Counter",(interval_bright?(bright_slices):(bright_slices/2)),0);
		gd2.addNumericField("Slice Start",yinit,0);
		gd2.addNumericField("Number of slices to reconstruct",yheight,0);
		gd2.addNumericField("Number of projections",nangles,0);
		gd2.addNumericField("Pixel size",pixSize,6);
		gd2.addNumericField("Insert Center Of Rotation",rotCenter,3);
		gd2.addNumericField("Smoothness parameter",smooth,3);
		gd2.addNumericField("Zinger parameter",zinger,3);
		gd2.addNumericField("Variance Estimation",1,0);
		gd2.addNumericField("Stop Threshold",10,0);
		gd2.addNumericField("Subsampling Factor",(int)nangles/256,0);
		gd2.addCheckbox("360 Rotation",recon360);
		gd2.addCheckbox("Brightfield Interval",interval_bright);
		Button saveScriptButton = new Button("Save MBIR Scripts");
		saveScriptButton.addActionListener(this);

		gd2.add(saveScriptButton);
		
		the_checkboxes2 = gd2.getCheckboxes();
		rotation_box = (Checkbox) the_checkboxes2.get(0);
		interval_box = (Checkbox) the_checkboxes2.get(0);
		//got some ideas for this from http://dev.loci.wisc.edu/trac/software/browser/trunk/components/loci-plugins/src/loci/plugins/importer/ImporterDialog.java?rev=6039#L403

		the_numbers2 = gd2.getNumericFields();
		width_field = (TextField) the_numbers2.get(0);
		resolution_field = (TextField) the_numbers2.get(1);
		dark_field = (TextField) the_numbers2.get(2);
		bright_field = (TextField) the_numbers2.get(3);
		start_field = (TextField) the_numbers2.get(4);
		slices_field = (TextField) the_numbers2.get(5);
		projection_field = (TextField) the_numbers2.get(6);
		pixel_field = (TextField) the_numbers2.get(7);
		rot_field = (TextField) the_numbers2.get(8);
		smooth_field = (TextField) the_numbers2.get(9);
		zinger_field = (TextField) the_numbers2.get(10);
		variance_field = (TextField) the_numbers2.get(11);
		threshold_field = (TextField) the_numbers2.get(12);
		subsampling_field = (TextField) the_numbers2.get(13);
		
		the_strings2 = gd2.getStringFields();
		input_field = (TextField) the_strings2.get(0);
		dataset_field  = (TextField) the_strings2.get(1);
		error_field  = (TextField) the_strings2.get(2);
		output_field  = (TextField) the_strings2.get(3);
		walltime_field  = (TextField) the_strings2.get(4);

	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("Save MBIR Scripts")){
			walltime = walltime_field.getText();
			xwidth = Integer.parseInt(width_field.getText());
			resolution = Integer.parseInt(resolution_field.getText());
			dark_slices = Integer.parseInt(dark_field.getText());
			bright_slices = Integer.parseInt(bright_field.getText());
			start = Integer.parseInt(start_field.getText());
			zelements = Integer.parseInt(slices_field.getText());
			nangles = Integer.parseInt(projection_field.getText());
			pixSize = Double.parseDouble(pixel_field.getText());
			rotCenter = Float.parseFloat(rot_field.getText());
			smooth = Float.parseFloat(smooth_field.getText());
			zinger = Float.parseFloat(zinger_field.getText());
			variance = Integer.parseInt(variance_field.getText());
			threshold = Integer.parseInt(threshold_field.getText());
			subsampling = Integer.parseInt(subsampling_field.getText());
			
			IJ.log("walltime = " + walltime);
			IJ.log("rootName = " + rootName);
			IJ.log("darkimg = "+ dark_slices);
			IJ.log("brightimg = "+ bright_slices);
			IJ.log("xwidth = "+ xwidth);
			IJ.log("resolution = "+ resolution);
			IJ.log("pixSize = "+ pixSize);
			IJ.log("rotationCenter = "+ rotCenter);
			IJ.log("zelements = "+ zelements);

			//next, we will use the arguments we got to start building the script;
			Writer writer = null;

			if (zelements%24>0){
				zelements=zelements-zelements%24;
			}
			
			IJ.log(lastDir+"32bit_"+rootName+".sh");
			IJ.log(lastDir+"run_"+rootName+".sh");
			IJ.log(lastDir+"test_"+rootName+".sh");

			try {
			    writer = new BufferedWriter(new OutputStreamWriter(
			          new FileOutputStream(lastDir+"32bit_"+rootName+".sh"), "utf-8"));
			    writer.write("python Bin2Tiff.py --input_path $SCRATCH/results/MBIR_smooth_"+Float.toString(smooth)+"_slice_start_"+Integer.toString(start)+"_slice_num_"+Integer.toString(zelements)+"_T_"+Float.toString(zinger)+"_r_1_K_1_N_theta_"+Integer.toString(nangles-1)+"_N_p_"+Integer.toString(256)+datasetName+"/ ");
			    writer.write("--output_tiff_path $SCRATCH/results/MBIR_smooth_"+Float.toString(smooth)+"_slice_start_"+Integer.toString(start)+"_slice_num_"+Integer.toString(zelements)+"_T_"+Float.toString(zinger)+"_r_1_K_1_N_theta_"+Integer.toString(nangles-1)+"_N_p_"+Integer.toString(256)+datasetName+"/ ");
			    writer.write("--tiff_base_name "+rootName+"_ ");
			    writer.write("--num_nodes "+Integer.toString(zelements/24)+" ");
			    writer.write("--z_start "+Integer.toString(start)+" ");
			    writer.write("--z_numElts "+Integer.toString(zelements)+" ");
			    writer.write("--im_width "+Integer.toString(resolution)+" ");
			    writer.write("--pix_size "+Double.toString(pixSize)+" ");
			} catch (IOException ex) {
			  // report
			} finally {
				try {writer.close();
				System.out.printf("32 Bit conversion script saved to %s!\n","32bit_"+rootName+".sh");
				} catch (Exception ex) {}
			}
			
			try {
			    writer = new BufferedWriter(new OutputStreamWriter(
			          new FileOutputStream(lastDir+"run_"+rootName+".sh"), "utf-8"));
			    writer.write("#/bin/tcsh\n");
			    writer.write("#PBS -q regular\n");
			    writer.write("#PBS -l mppwidth="+Integer.toString(zelements)+"\n");
			    writer.write("#PBS -l walltime="+walltime+"\n");
			    writer.write("#PBS -N "+rootName+"\n");
			    writer.write("#PBS -e "+rootName+".$PBS_JOBID.err\n");
			    writer.write("#PBS -o "+rootName+".$PBS_JOBID.out\n");
			    writer.write("#PBS -V\n");
			    writer.write("cd $PBS_O_WORKDIR\n");
			    writer.write("setenv OMP_NUM_THREADS 24\n");
			    writer.write("setenv CRAY_ROOTFS DSL\n");
			    writer.write("module load PrgEnv-intel\n");
			    writer.write("module load python/2.7.9\n");
			    writer.write("module load h5py\n");
			    writer.write("module load pil\n");
			    writer.write("module load mpi4py\n");
			    writer.write("python XT_MBIR_3D.py --setup_launch_folder ");
			    writer.write("--run_reconstruction --Edison --input_hdf5 $SCRATCH/data/"+rootName+".h5 ");
			    writer.write("--group_hdf5 /"+rootName+" --code_launch_folder $SCRATCH/launchfolder/ ");
			    writer.write("--output_hdf5 $SCRATCH/results/ ");
			    writer.write("--x_width "+Integer.toString(xwidth)+" --recon_x_width "+Integer.toString(resolution)+" ");
			    writer.write("--num_dark "+Integer.toString(dark_slices)+" --num_bright "+(interval_bright?Integer.toString(bright_slices):Integer.toString(bright_slices/2))+" ");
			    writer.write("--z_numElts "+Integer.toString(zelements)+" --z_start "+Integer.toString(start)+" --num_views "+Integer.toString(nangles-1)+" ");
			    writer.write("--pix_size "+Double.toString(pixSize)+" --rot_center "+Float.toString(rotCenter)+" --smoothness "+(lastAngle>180f?Float.toString((0.1f)):Float.toString((float)(smooth)))+" ");
			    writer.write("--zinger_thresh "+Float.toString(zinger)+" --Variance_Est 1 --stop_threshold 10 ");
			    writer.write("--num_threads 24 --num_nodes "+Integer.toString(zelements/24)+" --view_subsmpl_fact "+Integer.toString((nangles-1)/256)+" "+(recon360?"--full_rot":"")+" "+(interval_bright?"--inter_bright":"--dual_norm"));
			} catch (IOException ex) {
			  // report
			} 
			finally {
				try {writer.close();
				System.out.printf("Edison script saved to %s!\n","run_"+rootName+".sh");
				} catch (Exception ex) {}
			}
			try {
			    writer = new BufferedWriter(new OutputStreamWriter(
			          new FileOutputStream(lastDir+"test_"+rootName+".sh"), "utf-8"));
			    writer.write("#/bin/tcsh\n");
			    writer.write("#PBS -q regular\n");
			    writer.write("#PBS -l mppwidth=24\n");
			    writer.write("#PBS -l walltime="+walltime+"\n");
			    writer.write("#PBS -N "+rootName+"\n");
			    writer.write("#PBS -e "+rootName+".$PBS_JOBID.err\n");
			    writer.write("#PBS -o "+rootName+".$PBS_JOBID.out\n");
			    writer.write("#PBS -V\n");
			    writer.write("cd $PBS_O_WORKDIR\n");
			    writer.write("setenv OMP_NUM_THREADS 24\n");
			    writer.write("setenv CRAY_ROOTFS DSL\n");
			    writer.write("module load PrgEnv-intel\n");
			    writer.write("module load python/2.7.9\n");
			    writer.write("module load h5py\n");
			    writer.write("module load pil\n");
			    writer.write("module load mpi4py\n");
			    writer.write("python XT_MBIR_3D.py --setup_launch_folder ");
			    writer.write("--run_reconstruction --Edison --input_hdf5 $SCRATCH/data/"+rootName+".h5 ");
			    writer.write("--group_hdf5 /"+rootName+" --code_launch_folder $SCRATCH/launchfolder/ ");
			    writer.write("--output_hdf5 $SCRATCH/results/ ");
			    writer.write("--x_width "+Integer.toString(xwidth)+" --recon_x_width 2560 ");
			    writer.write("--num_dark "+Integer.toString(dark_slices)+" --num_bright "+Integer.toString(bright_slices)+" ");
			    writer.write("--z_numElts 24 --z_start "+Integer.toString(start)+" --num_views "+Integer.toString(nangles-1)+" ");
			    writer.write("--pix_size "+Double.toString(pixSize)+" --rot_center "+Float.toString(rotCenter)+" --smoothness "+(recon360?Float.toString((0.1f)):Float.toString((float)(smooth)))+" ");
			    writer.write("--zinger_thresh "+Float.toString(zinger)+" --Variance_Est 1 --stop_threshold 10 ");
			    writer.write("--num_threads 24 --num_nodes 1 --view_subsmpl_fact "+Integer.toString((nangles-1)/256)+" "+(recon360?"--full_rot":"")+" "+(interval_bright?"--inter_bright":"--dual_norm"));
			} catch (IOException ex) {
			  // report
			} 
			finally {
				try {writer.close();
				System.out.printf("Test Edison script saved to %s!\n","run_"+rootName+".sh");
				} catch (Exception ex) {}
			}
		}	
	}
}


class sctfilter implements FilenameFilter {
	public boolean accept(File dir, String name) {
		return (name.endsWith(".sct"));
	}
}
class tiffilter implements FilenameFilter {
	public boolean accept(File dir, String name) {
		return (name.endsWith(".tif"));
	}
}
class drkfilter implements FilenameFilter {
	public boolean accept(File dir, String name) {
		String thematch1 = ".+drk\\d+_\\d+\\.tif";
		String thematch2 = ".+drk_\\d+_\\d+\\.tif";
		return (name.matches(thematch1) || name.matches(thematch2));
	}
}
class bakfilter implements FilenameFilter {
	public boolean accept(File dir, String name) {
		String thematch1 = ".+bak\\d+_\\d+\\.tif";
		String thematch2 = ".+bak_\\d+_\\d+\\.tif";
		return (name.matches(thematch1) || name.matches(thematch2));
	}
}
class tifNOTbakNOTdrkfilter implements FilenameFilter {
	public boolean accept(File dir, String name) {
		String thematch1 = ".+bak\\d+_\\d+\\.tif";
		String thematch2 = ".+bak_\\d+_\\d+\\.tif";
		String thematch3 = ".+drk\\d+_\\d+\\.tif";
		String thematch4 = ".+drk_\\d+_\\d+\\.tif";
		return (name.endsWith(".tif") && !(name.matches(thematch1)) && !(name.matches(thematch2)) && !(name.matches(thematch3)) && !(name.matches(thematch4)));
	}
}

class csvfilter implements FilenameFilter {
	public boolean accept(File dir, String name) {
		return (name.endsWith(".csv"));
	}
}

class AttributeMap<Key, Value> {
	private Map<Key, Value> map;
	
	public AttributeMap(Map<Key, Value> map) {
		this.map = map;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		Iterator<Entry<Key, Value>> iter = map.entrySet().iterator();
		while (iter.hasNext()) {
			Entry<Key, Value> entry = iter.next();
			sb.append(entry.getKey());
			sb.append('=').append('"');
			sb.append(entry.getValue());
			sb.append('"');
			if (iter.hasNext()) {
				sb.append(',').append(' ');
			}
		}
		return sb.toString();
		
	}
}
